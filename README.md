# README #

1/3スケールのSEGA Master System風小物のstlファイルです。
スイッチ類、I/Oポート等は省略しています。

組み合わせたファイルとパーツごとに配置したファイルの二種類があります。
元ファイルはAUTODESK 123D DESIGNです。


***

# 実機情報

## メーカ
- SEGA

## 発売時期
- 1987年10月18日

## 参考資料

- [Wikipedia](https://ja.wikipedia.org/wiki/%E3%82%BB%E3%82%AC%E3%83%BB%E3%83%9E%E3%82%B9%E3%82%BF%E3%83%BC%E3%82%B7%E3%82%B9%E3%83%86%E3%83%A0)
- [セガハード大百科](https://sega.jp/history/hard/mastersystem/index.html)

***

![](https://bitbucket.org/kyoto-densouan/3dmodel_sega_master_system/raw/da96f3d3bdd17d8d4d32c71f5622aa9429467c4c/ModelView.png)
![](https://bitbucket.org/kyoto-densouan/3dmodel_sega_master_system/raw/da96f3d3bdd17d8d4d32c71f5622aa9429467c4c/ModelView_open.png)
![](https://bitbucket.org/kyoto-densouan/3dmodel_sega_master_system/raw/da96f3d3bdd17d8d4d32c71f5622aa9429467c4c/ExampleImage.png)
